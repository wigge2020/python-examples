# a2020-02
msg = "Hello World"                             # use variable msg
print(msg)                                      # print on screen
frst = 'albert'
last = 'einstein'
full_name = frst + ' ' + last                   # combining strings
print(full_name)
''' ------- output -------
 Hello World
 albert einstein
'''
